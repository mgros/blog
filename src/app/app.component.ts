import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  posts = [
    {
      title: 'Premier post',
      content: 'Contenu de mon 1er post'
    },
    {
      title: 'Deuxième post',
      content: 'Texte de mon 2e post'
    },
    {
      title: 'Dernier post',
      content: 'Message de mon dernier post'
    }
  ];
}
