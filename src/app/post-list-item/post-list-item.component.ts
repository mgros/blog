import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  constructor() {
    this.created_at = new Date();
    this.loveIts = 0;
  }

  ngOnInit() {
  }

  loveIt() {
    this.loveIts++;
  }

  dontLoveIt() {
    this.loveIts--;
  }

  @Input() title: string;
  @Input() content: string;
  loveIts: number;
  created_at: Date;
}
